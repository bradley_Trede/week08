package week08;


import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class hymnExecute {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean isWrongEntry;
        do {
            isWrongEntry = false;


            System.out.println("Enter hymn number (2, 4, 5): ");
            int getHymnNo = scanner.nextInt();


            ExecutorService myService = Executors.newFixedThreadPool(3);


            switch (getHymnNo) {
                case 2 -> {
                    chooseSong cs1 = new chooseSong("I am a child of God", getHymnNo, "I am a child of God", 2, 3750);
                    myService.execute(cs1);

                }
                case 4 -> {
                    chooseSong cs2 = new chooseSong("I lived in Heaven", getHymnNo, " I lived in heaven a long time ago, it is true", 4, 5750);
                    myService.execute(cs2);

                }
                case 5 -> {
                    chooseSong cs3 = new chooseSong("I Know My Father Lives", getHymnNo, " I know my Father lives and loves me too.", 5, 6750);
                    myService.execute(cs3);

                }
                default -> {
                   System.out.println("Enter hymn number (2, 4, 5): ");
                        isWrongEntry = true;

                }
            }
            myService.shutdown();
        } while (isWrongEntry);

    }
}