package week08;
 /*   Start Church Telepromptor
    Code using threads and runnables
    with a sleep command to time the
    verse with the average singing time. */

public class sleepTest {

    public static void main(String[] args) {
        System.out.println("Children's Hymn: I am a child of God");

        String[] messages = {"I am a child of God",
                "And he has sent me here",
                "Has given me an earthly home",
                "With Parents Near and Dear",
                "Lead me, Guide Me, Walk beside me",
                "Help me find the way",
                "Teach me all that I must do",
                "To live with him someday."};

        // Runnable
        Runnable runnable = () -> {
            System.out.println(" ");

            //For Loop
            for(String message: messages) {
                System.out.println(message);

                // handling with a thread sleep for the try.
                try {

                    //Determine the speed to sing the song and enter the verse time under sleep
                    Thread.sleep(3750);
                } catch (InterruptedException e) {
                    throw new IllegalStateException(e);
                }
            }
        };
        // execute the thread
        Thread thread = new Thread(runnable);

        thread.start();
    }
}


