package week08;

public class chooseSong implements Runnable {

    private String name;
    private String queueVerse;
    private int sleep;
    private int hymnNo;


    public chooseSong(String name, int hymnNo, String queueVerse, int i, int sleep) {

        this.name = name;
        this.hymnNo = hymnNo;
        this.queueVerse = queueVerse;
        this.sleep = sleep;

    }


    public void run() {
        System.out.println("The hymn for today is: " + name + " and is found on page " + hymnNo + " and the first verse is " + queueVerse);

        try {
            Thread.sleep(sleep);
        } catch (InterruptedException e) {

            System.out.println("\n" + "The tempo is " + sleep / 1000 + "seconds");
        }
    }
}